import java.util.ArrayList;


public class Binary {
    int f1;
    int f2;
    int product;

    public Binary(int f1, int f2){
        this.f1 = f1;
        this.f2 = f2;

        this.product=0;

        calculateProduct();

    }

    private ArrayList decToBin(){
        ArrayList<Boolean> digit = new ArrayList<>();
        int i = this.f2;
        while (i>0) {
            digit.add(i % 2 == 1);
            i /= 2;
        }
        return digit;
    }

    private void calculateProduct(){
        ArrayList<Boolean> digit = this.decToBin();
        int m = this.f1;
        for(int i =0;i<digit.size();i++){
            if(digit.get(i)){
                this.product += m;
            }
            m+=m;
        }
    }
}

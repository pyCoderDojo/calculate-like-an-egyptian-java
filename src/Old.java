import java.util.ArrayList;

public class Old {
    int f1;
    int f2;
    int product;
    ArrayList<Integer> left;
    ArrayList<Integer> right;

    public Old(int f1,int f2){
        this.f1=f1;
        this.f2=f2;

        this.product=0;

        calculate();

    }

    private void calculate(){

        //Initialisieren
        left = new ArrayList<Integer>();
        right = new ArrayList<Integer>();

        //jeweils die ersten Werte hinzufügen, damit diese dann in der Schleife verdoppelt werden können
        left.add(f1);
        right.add(1);

        //letzten Wert verdoppeln und hinten an die List hängen
        while(right.get(right.size()-1)+right.get(right.size()-1)<=f2){
            left.add(left.get(left.size()-1)+left.get(left.size()-1));
            right.add(right.get(right.size()-1)+right.get(right.size()-1));
        }


        int s =0;
        for(int i=right.size()-1;i>=0;i--){
            if(s+right.get(i)<=f2){
                s+=right.get(i);
                this.product+=left.get(i);
            }
        }
    }
}



